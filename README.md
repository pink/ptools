# ptools

A few EPICS subroutines

[[_TOC_]]

## Usage

### Linspace
Creates an array from A to B with C number of points.

outrecord = linspace(A,B,C)
- A: start
- B: end
- C: number of points

``` bash
record(aSub, "$(P):$(R)")
{
  field(SNAM, "linspace")
  field(INPA, "start")
  field(INPB, "end")
  field(INPC, "Number of points")
  field(OUTA, "Output link")
}
```
### Range
Creates an array from A to B (inclusive) with unit increments

outrecord = range(A,B)
- A: start
- B: end
- C: Output waveform NELM

``` bash
record(aSub, "$(P):$(R)")
{
  field(SNAM, "range")
  field(INPA, "start")
  field(INPB, "end")
  field(INPC, "Output record.NELM")
  field(OUTA, "Output record")
}
```
### calcspec
Multiplies array elements by a constant. (Created to fix the averaged output of ROI profiles on AreaDetector)

- INPA: Input waveform
- INPB: ROI height 
- INPD: Length of array
- OUTA: Output waveform

``` bash
record(aSub, "$(BL):$(DEV):x:$(N):calc_specX")
{
  field(SNAM, "calcspec")
  field(INPA, "$(BL):$(DEV):Stats$(STATS):ProfileAverageX_RBV CPP")
  field(FTA, "DOUBLE")
  field(NOA, "$(SIZE)")
  field(INPB, "$(BL):$(DEV):Stats$(STATS):ArraySize1_RBV")
  field(FTB, "DOUBLE")
  field(NOB, "1")
  field(INPD, "$(BL):$(DEV):Stats$(STATS):ProfileAverageX_RBV.NORD")
  field(OUTA, "$(BL):$(DEV):x:$(N):specX_RBV PP")
  field(FTVA, "DOUBLE")
  field(NOVA, "$(SIZE)")
}
```

### aavg
Creates averaged array and keeps track of number of averaged arrays

- INPA: Input waveform
- INPB: Reset signal (0:reset, 1:enable) 
- INPC: Input waveform.NORD or array length
- OUTA: Output waveform
- OUTB: Number of averaged arrays

```bash
record(aSub, "$(P):specavg_calc")
{
  field(SNAM, "aavg")
  field(INPA, "$(P):specsum_RBV CPP")
  field(FTA, "DOUBLE")
  field(NOA, "$(SIZE)")
  field(INPB, "$P):reset_inv CPP")
  field(FTB, "DOUBLE")
  field(NOB, "1")
  field(INPC, "$(P):specsum_RBV.NORD")
  field(FTC, "DOUBLE")
  field(NOC, "1")
  field(OUTA, "$(P):specsum_avg_RBV PP")
  field(FTVA, "DOUBLE")
  field(NOVA, "$(SIZE)")
  field(OUTB, "$(P):N_avg_RBV PP")
  field(FTVB, "DOUBLE")
  field(NOVB, "1")
}
```
